package problem2;

public class SubstringForKids {

	public static void main(String[] args) {
		System.out.println("SUBSTRING FOR KIDS");
		// When i ==j
		System.out.println(substringForKids(1, 1, "fun"));
		// When i < j
		System.out.println(substringForKids(0, 5, "peterisshouting"));
		System.out.println(substringForKids(2, 4, "apple"));
		// When i > j
		System.out.println(substringForKids(5, 3, "bad"));
		// When i or j is negative
		System.out.println(substringForKids(-1, 3, "fun"));
	}

	public static String substringForKids(int i, int j, String sentence) {
		String res = "You gave me invalid numbers!";
		if (i == j) {
			res = "" + sentence.charAt(i);
		} else if (i < 0 || j < 0 || i > j) {
			// do nothing because res is already what we want.
		} else if (i < j) {
			res = "";
			for (int k = i; k <= j; k++) {
				res += "" + sentence.charAt(k);
			}
		}
		return res;
	}
}
