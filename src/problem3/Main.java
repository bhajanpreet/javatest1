package problem3;

public class Main {
	public static void main(String[] args) throws InterruptedException {
		Rabbit rabbit = new Rabbit(225, 100);
		rabbit.sayHello();

		boolean runForever = true;

		final int X_START_POS = 0;
		final int X_END_POS = 500;
		final int MOVEMENT_STEP = 15;

		String directionOfMovement = "LEFT";
		while (runForever == true) {
			System.out.println("Position: x -> " + rabbit.getxPosition() + " y -> " + rabbit.getyPosition());
			if (directionOfMovement.equals("LEFT")) {
				rabbit.setxPosition(rabbit.getxPosition() - MOVEMENT_STEP);

			} else if (directionOfMovement.equals("RIGHT")) {
				rabbit.setxPosition(rabbit.getxPosition() + MOVEMENT_STEP);
			}

			if ((rabbit.getxPosition() - X_START_POS) < MOVEMENT_STEP) {
				directionOfMovement = "RIGHT";
			} else if ((X_END_POS - rabbit.getxPosition()) < MOVEMENT_STEP) {
				directionOfMovement = "LEFT";
			}
			Thread.sleep(1000);
		}
	}

}
