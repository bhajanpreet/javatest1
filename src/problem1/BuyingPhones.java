package problem1;

import java.util.Scanner;

public class BuyingPhones {

	public static void main(String[] args) {
		System.out.println("Enter the regular price of a phone: ");
		Scanner s = new Scanner(System.in);
		double price = s.nextDouble();

		double price1 = price;
		double price2 = price;
		double price3 = price / 2;

		System.out.println("Phone 1 - $" + price1);
		System.out.println("Phone 2 - $" + price2);
		System.out.println("Phone 3 - $" + price3);
		s.close();
	}

}
